﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class ShowBlackScreen : BaseTask
    {
        private readonly SimpleDelegate _onShowBlackScreenComplete;

        public ShowBlackScreen()
        {
            _onShowBlackScreenComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.ShowBlackScreen);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(CommonEvent.ShowBlackScreenComplete, _onShowBlackScreenComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.ShowBlackScreenComplete, _onShowBlackScreenComplete);
        }
    }
}