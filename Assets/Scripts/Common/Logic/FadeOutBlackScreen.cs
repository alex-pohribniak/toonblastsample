﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;

namespace Common.Logic
{
    public class FadeOutBlackScreen : BaseTask
    {
        private readonly SimpleDelegate _onFadeOutBlackScreenComplete;

        public FadeOutBlackScreen()
        {
            _onFadeOutBlackScreenComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(CommonEvent.FadeOutBlackScreen);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(CommonEvent.FadeOutBlackScreenComplete, _onFadeOutBlackScreenComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(CommonEvent.FadeOutBlackScreenComplete, _onFadeOutBlackScreenComplete);
        }
    }
}