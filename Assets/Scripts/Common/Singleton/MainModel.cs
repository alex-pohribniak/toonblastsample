﻿using System;
using ToonBlastSample.Model;
using UnityEngine;

namespace Common.Singleton
{
    public class MainModel : MonoBehaviour
    {
        private static MainModel _mainModel;
        private ToonBlastSampleData _toonBlastSampleData;

        private static MainModel Instance
        {
            get
            {
                if (_mainModel != null) return _mainModel;
                _mainModel = FindObjectOfType(typeof(MainModel)) as MainModel;
                if (_mainModel == null)
                    throw new Exception("There needs to be one active Model script in your scene.");
                _mainModel.Init();

                return _mainModel;
            }
        }

        public static ToonBlastSampleData ToonBlastSampleData
        {
            get => Instance._toonBlastSampleData;
            set => Instance._toonBlastSampleData = value;
        }

        public static bool IsNull()
        {
            return _mainModel == null;
        }

        private void Init()
        {
            if (_toonBlastSampleData == null) _toonBlastSampleData = new ToonBlastSampleData();
        }
    }
}