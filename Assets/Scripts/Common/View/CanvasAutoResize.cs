﻿using UnityEngine;
using UnityEngine.UI;

namespace Common.View
{
    public class CanvasAutoResize : MonoBehaviour
    {
        private void OnEnable()
        {
            GetComponent<CanvasScaler>().matchWidthOrHeight = Screen.width / (float) Screen.height > 9 / 16.0f ? 1 : 0;
        }
    }
}