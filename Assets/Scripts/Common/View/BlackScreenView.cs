﻿using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

namespace Common.View
{
    public class BlackScreenView : MonoBehaviour
    {
        public Image blackScreen;
        private readonly SimpleDelegate _onFadeOutBlackScreen;
        private readonly SimpleDelegate _onShowBlackScreen;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public BlackScreenView()
        {
            _onShowBlackScreen = OnShowBlackScreen;
            _onFadeOutBlackScreen = OnFadeOutBlackScreen;
        }

        protected void Awake()
        {
            blackScreen.gameObject.SetActive(false);

            Observer.AddListener(CommonEvent.ShowBlackScreen, _onShowBlackScreen);
            Observer.AddListener(CommonEvent.FadeOutBlackScreen, _onFadeOutBlackScreen);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;

            Observer.RemoveListener(CommonEvent.ShowBlackScreen, _onShowBlackScreen);
            Observer.RemoveListener(CommonEvent.FadeOutBlackScreen, _onFadeOutBlackScreen);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowBlackScreen()
        {
            blackScreen.gameObject.SetActive(true);
            blackScreen.color = Color.black;
            ClearTween();
            _tweenDuration = 1f;
            _tweenAnimation = DOTween.Sequence();
            var tweenFade = blackScreen.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() => { Observer.Emit(CommonEvent.ShowBlackScreenComplete); });
            _tweenAnimation.Play();
        }

        private void OnFadeOutBlackScreen()
        {
            ClearTween();
            _tweenDuration = 1f;
            _tweenAnimation = DOTween.Sequence();
            var tweenFade = blackScreen.DOFade(0f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() =>
            {
                blackScreen.gameObject.SetActive(false);
                Observer.Emit(CommonEvent.FadeOutBlackScreenComplete);
            });
            _tweenAnimation.Play();
        }
    }
}