﻿using ToonBlastSample.Model;
using UnityEngine;

namespace ToonBlastSample.Config
{
    public class ToonBlastSampleConfig : MonoBehaviour
    {
        public ToonBlastSampleData toonBlastSampleData;
    }
}