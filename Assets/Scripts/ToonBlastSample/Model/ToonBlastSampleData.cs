﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace ToonBlastSample.Model
{
    [Serializable]
    public class ToonBlastSampleData
    {
        public Dictionary<float, int> destroyedBlocks;
        public int blocksInChain;
        public List<Color> colorsForBlocks;
        public int columns;
        public int rows;
        public int actionsToWin;
        public int currentPoints;
        public int pointsToWin;
        public int pointsForChain;
        public int positionInChain;
        public int rangeValue;
        public int blockPointValue;
        public int maxBlockPointValueAboveRange;
        public bool win;
    }
}