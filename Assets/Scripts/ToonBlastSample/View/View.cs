﻿using Common.Enums;
using Common.Singleton;
using ToonBlastSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace ToonBlastSample.View
{
    public class View : MonoBehaviour
    {
        private readonly SimpleDelegate _onUpdateView;
        public Text actionsCounter;

        public Text currentPoints;
        public Slider pointsSlider;
        public Text pointsToWin;

        public View()
        {
            _onUpdateView = OnUpdateView;
        }

        private void OnUpdateView()
        {
            currentPoints.text = MainModel.ToonBlastSampleData.currentPoints.ToString();
            pointsToWin.text = MainModel.ToonBlastSampleData.pointsToWin.ToString();
            pointsSlider.value = MainModel.ToonBlastSampleData.currentPoints;
            pointsSlider.maxValue = MainModel.ToonBlastSampleData.pointsToWin;
            actionsCounter.text = MainModel.ToonBlastSampleData.actionsToWin.ToString();
        }

        private void Awake()
        {
            Observer.AddListener(ToonBlastSampleEvent.UpdateView, _onUpdateView);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.UpdateView, _onUpdateView);
        }
    }
}