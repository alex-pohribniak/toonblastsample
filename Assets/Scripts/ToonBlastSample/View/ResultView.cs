﻿using Common.Enums;
using Common.Singleton;
using DG.Tweening;
using ToonBlastSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace ToonBlastSample.View
{
    public class ResultView : MonoBehaviour
    {
        public Image result;
        private readonly SimpleDelegate _onShowResult;
        private Sequence _tweenAnimation;
        private float _tweenDuration;

        public ResultView()
        {
            _onShowResult = OnShowResult;
        }

        protected void Awake()
        {
            result.gameObject.SetActive(false);
            Observer.AddListener(ToonBlastSampleEvent.ShowResult, _onShowResult);
        }

        protected void OnDestroy()
        {
            ClearTween();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.ShowResult, _onShowResult);
        }

        private void ClearTween()
        {
            if (_tweenAnimation == null) return;
            _tweenAnimation.Kill();
            _tweenAnimation.onComplete = null;
            _tweenAnimation = null;
        }

        private void OnShowResult()
        {
            result.gameObject.SetActive(true);
            ClearTween();
            _tweenDuration = 3f;
            _tweenAnimation = DOTween.Sequence();
            result.sprite = DefineResultSprite();
            var tweenFade = result.DOFade(1f, _tweenDuration);
            _tweenAnimation.Join(tweenFade);
            _tweenAnimation.OnComplete(() => { Observer.Emit(ToonBlastSampleEvent.ShowResultComplete); });
            _tweenAnimation.Play();
        }

        private static Sprite DefineResultSprite()
        {
            if (MainModel.ToonBlastSampleData.win) return Resources.Load<Sprite>("Common/Sprites/Win");
            return !MainModel.ToonBlastSampleData.win ? Resources.Load<Sprite>("Common/Sprites/GameOver") : default;
        }
    }
}