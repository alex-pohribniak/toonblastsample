﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace ToonBlastSample.View
{
    public class HitArea : MonoBehaviour, IPointerClickHandler
    {
        public Camera uiCamera;

        public void OnPointerClick(PointerEventData eventData)
        {
            var position = uiCamera.ScreenToWorldPoint(eventData.position);
            var hit = Physics2D.Raycast(position, Vector3.back);
            if (hit) hit.collider.gameObject.SendMessage("Strike");
        }
    }
}