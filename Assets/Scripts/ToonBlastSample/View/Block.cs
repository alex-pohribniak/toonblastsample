﻿using Common.Singleton;
using DG.Tweening;
using ToonBlastSample.Enums;
using UnityEngine;
using UnityEngine.UI;

namespace ToonBlastSample.View
{
    public class Block : MonoBehaviour
    {
        private Color _blockColor;

        public ParticleSystem destroy;
        public Text scores;
        public ParticleSystem scoringParticle;

        private void Strike()
        {
            var arguments = new object[] {_blockColor, true};
            Observer.Emit(ToonBlastSampleEvent.ChainReaction);
            StartChainReaction(arguments);
        }

        private void StartChainReaction(object args)
        {
            var argsArray = (object[]) args;
            var blockColor = (Color) argsArray[0];
            var firstElement = (bool) argsArray[1];

            if (!_blockColor.Equals(blockColor)) return;

            Observer.Emit(ToonBlastSampleEvent.ProceedChainReaction);

            var arguments = new object[] {_blockColor, false};
            var position = GetComponent<BoxCollider2D>().bounds.center;
            var height = gameObject.GetComponent<RectTransform>().rect.height;
            var width = gameObject.GetComponent<RectTransform>().rect.width;
            gameObject.GetComponent<BoxCollider2D>().enabled = false;

            var hitUp = Physics2D.Raycast(position, Vector3.up, height);
            if (hitUp)
                hitUp.collider.gameObject.SendMessage("StartChainReaction", arguments,
                    SendMessageOptions.DontRequireReceiver);

            var hitDown = Physics2D.Raycast(position, Vector3.down, height);
            if (hitDown)
                hitDown.collider.gameObject.SendMessage("StartChainReaction", arguments,
                    SendMessageOptions.DontRequireReceiver);

            var hitRight = Physics2D.Raycast(position, Vector3.right, width);
            if (hitRight)
                hitRight.collider.gameObject.SendMessage("StartChainReaction", arguments,
                    SendMessageOptions.DontRequireReceiver);

            var hitLeft = Physics2D.Raycast(position, Vector3.left, width);
            if (hitLeft)
                hitLeft.collider.gameObject.SendMessage("StartChainReaction", arguments,
                    SendMessageOptions.DontRequireReceiver);

            Observer.Emit(ToonBlastSampleEvent.TrackDestroyedBlock, gameObject.transform.position.x);
            MainModel.ToonBlastSampleData.blocksInChain++;
            if (firstElement && MainModel.ToonBlastSampleData.blocksInChain == 1)
            {
                gameObject.GetComponent<BoxCollider2D>().enabled = true;
            }
            else
            {
                Destroy(gameObject);
                AnimateDestroy();
                AnimateScoring();
            }

            if (firstElement) Observer.Emit(ToonBlastSampleEvent.ChainReactionComplete);
        }

        private void AnimateDestroy()
        {
            var blockCenter = transform.TransformPoint(gameObject.GetComponent<RectTransform>().rect.center);
            var destroyClone = Instantiate(destroy, blockCenter, Quaternion.identity, gameObject.transform.parent);
            destroyClone.Play();
            Destroy(destroyClone, 1f);
        }

        private void AnimateScoring()
        {
            var sequence = DOTween.Sequence();
            var blockCenter = transform.TransformPoint(gameObject.GetComponent<RectTransform>().rect.center);
            var particleClone = Instantiate(scoringParticle, blockCenter, Quaternion.identity,
                gameObject.transform.parent);
            var scoresPosition = scores.gameObject.transform.position;
            const float duration = 1f;
            var tweenMove = particleClone.transform.DOMove(scoresPosition, duration);
            sequence.Join(tweenMove);
            sequence.OnComplete(() =>
            {
                Destroy(particleClone);
                sequence?.Kill();
                sequence = null;
            });
            sequence.Play();
            particleClone.Play();
        }

        private void Awake()
        {
            _blockColor = gameObject.GetComponent<SpriteRenderer>().color;
        }
    }
}