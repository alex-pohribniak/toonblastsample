﻿using System.Collections;
using Common.Enums;
using Common.Singleton;
using ToonBlastSample.Enums;
using UnityEngine;

namespace ToonBlastSample.View
{
    public class BlocksArea : MonoBehaviour
    {
        private readonly SimpleDelegate _onCreateNewBlocks;
        private readonly SimpleDelegate _onInitBlocksArea;
        private float _blockHeight;
        private Rect _blocksAreaRect;
        private float _blockWidth;

        private int _columns;
        private int _rows;
        private float _spawnAreaHeight;

        public GameObject blocksArea;
        public GameObject blockTemplate;
        public GameObject spawnArea;

        public BlocksArea()
        {
            _onCreateNewBlocks = OnCreateNewBlocks;
            _onInitBlocksArea = OnInitBlocksArea;
        }

        private void OnCreateNewBlocks()
        {
            var destroyedBlocks = MainModel.ToonBlastSampleData.destroyedBlocks;

            foreach (var key in destroyedBlocks.Keys)
                for (var i = 0; i < destroyedBlocks[key]; i++)
                    CreateBlock(key);

            Observer.Emit(ToonBlastSampleEvent.CreateNewBlocksComplete);
        }

        private void OnInitBlocksArea()
        {
            StartCoroutine(InitBlocksArea());
            Observer.Emit(ToonBlastSampleEvent.InitBlocksAreaComplete);
        }

        private IEnumerator InitBlocksArea()
        {
            var wait = new WaitForSeconds(0.05f);
            for (var i = 0; i < _rows; i++)
            for (var j = 0; j < _columns; j++)
            {
                CreateBlock(j);
                yield return wait;
            }
        }

        private void CreateBlock(int xCoordinateMultiplier)
        {
            var spawnPosition = new Vector3(xCoordinateMultiplier * _blockWidth, _spawnAreaHeight, 1f);
            var blockClone = Instantiate(blockTemplate, blockTemplate.transform.parent);
            blockClone.transform.position = blockClone.transform.TransformPoint(spawnPosition);
            var blockCloneRect = blockClone.GetComponent<RectTransform>().rect;
            blockCloneRect.width = _blockWidth;
            blockCloneRect.height = _blockHeight;
            var blockTemplateRect = blockTemplate.GetComponent<RectTransform>().rect;
            var scaleValue = new Vector2(_blockWidth / blockTemplateRect.width,
                _blockHeight / blockTemplateRect.height);
            blockClone.transform.localScale = scaleValue;
            blockClone.GetComponent<SpriteRenderer>().color = GetRandomColorFromAvailable();
            blockClone.GetComponent<BoxCollider2D>().size -= Vector2.one;
            blockClone.SetActive(true);
        }

        private void CreateBlock(float xCoordinate)
        {
            var spawnPosition = new Vector3(xCoordinate, spawnArea.transform.position.y, 1f);
            var blockClone = Instantiate(blockTemplate, blockTemplate.transform.parent);
            blockClone.transform.position = spawnPosition;
            var blockCloneRect = blockClone.GetComponent<RectTransform>().rect;
            blockCloneRect.width = _blockWidth;
            blockCloneRect.height = _blockHeight;
            var blockTemplateRect = blockTemplate.GetComponent<RectTransform>().rect;
            var scaleValue = new Vector2(_blockWidth / blockTemplateRect.width,
                _blockHeight / blockTemplateRect.height);
            blockClone.transform.localScale = scaleValue;
            blockClone.GetComponent<SpriteRenderer>().color = GetRandomColorFromAvailable();
            blockClone.GetComponent<BoxCollider2D>().size -= Vector2.one;
            blockClone.SetActive(true);
        }

        private Color GetRandomColorFromAvailable()
        {
            var colorsForBlocks = MainModel.ToonBlastSampleData.colorsForBlocks;
            return colorsForBlocks[Random.Range(0, colorsForBlocks.Count)];
        }

        private void InitPrivates()
        {
            _columns = MainModel.ToonBlastSampleData.columns;
            _rows = MainModel.ToonBlastSampleData.rows;
            _spawnAreaHeight = spawnArea.GetComponent<RectTransform>().rect.height;
            _blocksAreaRect = blocksArea.GetComponent<RectTransform>().rect;
            _blockWidth = _blocksAreaRect.width / _columns;
            _blockHeight = _blocksAreaRect.height / _rows;
        }

        private void Awake()
        {
            Observer.AddListener(ToonBlastSampleEvent.InitBlocksArea, _onInitBlocksArea);
            Observer.AddListener(ToonBlastSampleEvent.CreateNewBlocks, _onCreateNewBlocks);

            blockTemplate.SetActive(false);

            Invoke(nameof(InitPrivates), 1f);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.CreateNewBlocks, _onCreateNewBlocks);
            Observer.RemoveListener(ToonBlastSampleEvent.InitBlocksArea, _onInitBlocksArea);
        }
    }
}