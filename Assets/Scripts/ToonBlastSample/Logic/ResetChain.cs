﻿using Common.Singleton;
using Common.Tasks;

namespace ToonBlastSample.Logic
{
    public class ResetChain : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ToonBlastSampleData.positionInChain = 0;
            MainModel.ToonBlastSampleData.rangeValue = 5;
            MainModel.ToonBlastSampleData.blockPointValue = 0;
            MainModel.ToonBlastSampleData.pointsForChain = 0;
            MainModel.ToonBlastSampleData.maxBlockPointValueAboveRange = 30;
            MainModel.ToonBlastSampleData.destroyedBlocks.Clear();
            MainModel.ToonBlastSampleData.blocksInChain = 0;
            Complete();
        }
    }
}