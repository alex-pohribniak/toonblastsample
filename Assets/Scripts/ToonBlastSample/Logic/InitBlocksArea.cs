﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class InitBlocksArea : BaseTask
    {
        private readonly SimpleDelegate _onInitBlocksAreaComplete;

        public InitBlocksArea()
        {
            _onInitBlocksAreaComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ToonBlastSampleEvent.InitBlocksArea);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(ToonBlastSampleEvent.InitBlocksAreaComplete, _onInitBlocksAreaComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.InitBlocksAreaComplete, _onInitBlocksAreaComplete);
        }
    }
}