﻿using Common.Tasks;
using UnityEngine.SceneManagement;

namespace ToonBlastSample.Logic
{
    public class EndToonBlast : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            SceneManager.LoadScene("ToonBlastSample");
            Complete();
        }
    }
}