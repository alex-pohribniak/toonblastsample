﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class CreateNewBlocks : BaseTask
    {
        private readonly SimpleDelegate _onCreateNewBlocksComplete;

        public CreateNewBlocks()
        {
            _onCreateNewBlocksComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ToonBlastSampleEvent.CreateNewBlocks);
        }

        protected override bool Guard()
        {
            return MainModel.ToonBlastSampleData.blocksInChain > 1;
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(ToonBlastSampleEvent.CreateNewBlocksComplete, _onCreateNewBlocksComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.CreateNewBlocksComplete, _onCreateNewBlocksComplete);
        }
    }
}