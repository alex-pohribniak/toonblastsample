﻿using System.Collections.Generic;
using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Config;

namespace ToonBlastSample.Logic
{
    public class InitToonBlast : BaseTask
    {
        public ToonBlastSampleConfig toonBlastSampleConfig;

        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ToonBlastSampleData = toonBlastSampleConfig.toonBlastSampleData;
            MainModel.ToonBlastSampleData.destroyedBlocks = new Dictionary<float, int>();
            Complete();
        }
    }
}