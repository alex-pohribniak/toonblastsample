﻿using Common.Singleton;
using Common.Tasks;

namespace ToonBlastSample.Logic
{
    public class ReduceAction : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ToonBlastSampleData.actionsToWin--;
            Complete();
        }

        protected override bool Guard()
        {
            return MainModel.ToonBlastSampleData.blocksInChain > 1;
        }
    }
}