﻿using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class CheckPoints : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            if (MainModel.ToonBlastSampleData.currentPoints >= MainModel.ToonBlastSampleData.pointsToWin)
            {
                MainModel.ToonBlastSampleData.win = true;
                Observer.Emit(ToonBlastSampleEvent.PlayerWon);
            }

            Complete();
        }
    }
}