﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class ShowResult : BaseTask
    {
        private readonly SimpleDelegate _onShowResultComplete;

        public ShowResult()
        {
            _onShowResultComplete = Complete;
        }

        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ToonBlastSampleEvent.ShowResult);
        }

        protected override void Activate()
        {
            base.Activate();
            Observer.AddListener(ToonBlastSampleEvent.ShowResultComplete, _onShowResultComplete);
        }

        protected override void DeActivate()
        {
            base.DeActivate();
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.ShowResultComplete, _onShowResultComplete);
        }
    }
}