﻿using Common.Enums;
using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;
using UnityEngine;

namespace ToonBlastSample.Logic
{
    public class ToonBlastSampleLogic : MonoBehaviour
    {
        private readonly SimpleDelegate _onChainReaction;
        private readonly SimpleDelegate _onChainReactionComplete;
        private readonly SimpleDelegate _onPlayerLost;
        private readonly SimpleDelegate _onPlayerWon;
        private readonly SimpleDelegate _onProceedChainReaction;
        private readonly FloatDelegate _onTrackDestroyedBlock;

        public BaseTask chainReaction;
        public BaseTask chainReactionComplete;
        public BaseTask endToonBlastSample;
        public BaseTask proceedChainReaction;
        public BaseTask startTask;

        public ToonBlastSampleLogic()
        {
            _onTrackDestroyedBlock = OnTrackDestroyedBlock;
            _onProceedChainReaction = OnProceedChainReaction;
            _onChainReactionComplete = OnChainReactionComplete;
            _onChainReaction = OnChainReaction;
            _onPlayerLost = OnPlayerLost;
            _onPlayerWon = OnPlayerWon;
        }

        private void OnTrackDestroyedBlock(float value)
        {
            var dictionary = MainModel.ToonBlastSampleData.destroyedBlocks;
            if (dictionary.ContainsKey(value))
            {
                dictionary[value]++;
            }
            else
            {
                dictionary.Add(value, 1);
            }
        }

        private void OnProceedChainReaction()
        {
            proceedChainReaction.Execute();
        }

        private void OnChainReactionComplete()
        {
            chainReactionComplete.Execute();
        }

        private void OnChainReaction()
        {
            chainReaction.Execute();
        }

        private void OnPlayerLost()
        {
            endToonBlastSample.Execute();
        }

        private void OnPlayerWon()
        {
            endToonBlastSample.Execute();
        }

        private void Start()
        {
            startTask.Execute();
        }

        private void Awake()
        {
            Observer.AddListener(ToonBlastSampleEvent.PlayerWon, _onPlayerWon);
            Observer.AddListener(ToonBlastSampleEvent.PlayerLost, _onPlayerLost);
            Observer.AddListener(ToonBlastSampleEvent.ChainReaction, _onChainReaction);
            Observer.AddListener(ToonBlastSampleEvent.ChainReactionComplete, _onChainReactionComplete);
            Observer.AddListener(ToonBlastSampleEvent.ProceedChainReaction, _onProceedChainReaction);
            Observer.AddListener(ToonBlastSampleEvent.TrackDestroyedBlock, _onTrackDestroyedBlock);
        }

        private void OnDestroy()
        {
            if (Observer.IsNull()) return;
            Observer.RemoveListener(ToonBlastSampleEvent.PlayerWon, _onPlayerWon);
            Observer.RemoveListener(ToonBlastSampleEvent.PlayerLost, _onPlayerLost);
            Observer.RemoveListener(ToonBlastSampleEvent.ChainReaction, _onChainReaction);
            Observer.RemoveListener(ToonBlastSampleEvent.ChainReactionComplete, _onChainReactionComplete);
            Observer.RemoveListener(ToonBlastSampleEvent.ProceedChainReaction, _onProceedChainReaction);
            Observer.RemoveListener(ToonBlastSampleEvent.TrackDestroyedBlock, _onTrackDestroyedBlock);
        }
    }
}