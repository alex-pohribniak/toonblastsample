﻿using System;
using Common.Singleton;
using Common.Tasks;

namespace ToonBlastSample.Logic
{
    public class ProceedChainReaction : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();

            MainModel.ToonBlastSampleData.positionInChain++;
            
            if (MainModel.ToonBlastSampleData.positionInChain <= MainModel.ToonBlastSampleData.rangeValue)
            {
                MainModel.ToonBlastSampleData.blockPointValue = 0;
                MainModel.ToonBlastSampleData.pointsForChain = 0;

                MainModel.ToonBlastSampleData.blockPointValue =
                    MainModel.ToonBlastSampleData.positionInChain *
                    (MainModel.ToonBlastSampleData.positionInChain - 1) -
                    (MainModel.ToonBlastSampleData.positionInChain - 1) +
                    Math.Max(MainModel.ToonBlastSampleData.positionInChain - 2, 0) +
                    (MainModel.ToonBlastSampleData.positionInChain * Math.Max(MainModel.ToonBlastSampleData.positionInChain - 3, 0)) +
                    Math.Max(MainModel.ToonBlastSampleData.positionInChain - 4, 0);
            }
            else
                MainModel.ToonBlastSampleData.blockPointValue =
                    MainModel.ToonBlastSampleData.maxBlockPointValueAboveRange;

            MainModel.ToonBlastSampleData.pointsForChain += MainModel.ToonBlastSampleData.blockPointValue;
            Complete();
        }
    }
}