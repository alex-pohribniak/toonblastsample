﻿using Common.Singleton;
using Common.Tasks;

namespace ToonBlastSample.Logic
{
    public class CalculateCurrentPoints : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            MainModel.ToonBlastSampleData.currentPoints += MainModel.ToonBlastSampleData.pointsForChain;
            Complete();
        }
    }
}