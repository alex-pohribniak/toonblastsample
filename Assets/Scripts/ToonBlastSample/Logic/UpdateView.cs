﻿using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class UpdateView : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Observer.Emit(ToonBlastSampleEvent.UpdateView);
            Complete();
        }
    }
}