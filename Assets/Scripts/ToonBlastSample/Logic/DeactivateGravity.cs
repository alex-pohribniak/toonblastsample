﻿using Common.Tasks;
using UnityEngine;

namespace ToonBlastSample.Logic
{
    public class DeactivateGravity : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            Physics2D.gravity = Vector2.zero;
            Complete();
        }
    }
}