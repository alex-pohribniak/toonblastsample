﻿using Common.Singleton;
using Common.Tasks;
using ToonBlastSample.Enums;

namespace ToonBlastSample.Logic
{
    public class CheckActions : BaseTask
    {
        protected override void OnExecute()
        {
            base.OnExecute();
            if (MainModel.ToonBlastSampleData.actionsToWin <= 0)
            {
                MainModel.ToonBlastSampleData.win = false;
                Observer.Emit(ToonBlastSampleEvent.PlayerLost);
            }

            Complete();
        }
    }
}