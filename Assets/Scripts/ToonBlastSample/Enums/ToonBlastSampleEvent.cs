﻿namespace ToonBlastSample.Enums
{
    public static class ToonBlastSampleEvent
    {
        public const string PlayerWon = "PlayerWon";
        public const string PlayerLost = "PlayerLost";
        public const string ShowResult = "ShowResult";
        public const string ShowResultComplete = "ShowResultComplete";
        public const string InitBlocksArea = "InitBlocksArea";
        public const string InitBlocksAreaComplete = "InitBlocksAreaComplete";
        public const string ChainReaction = "ChainReaction";
        public const string ChainReactionComplete = "ChainReactionComplete";
        public const string ProceedChainReaction = "ProceedChainReaction";
        public const string TrackDestroyedBlock = "TrackDestroyedBlock";
        public const string CreateNewBlocks = "CreateNewBlocks";
        public const string CreateNewBlocksComplete = "CreateNewBlocksComplete";
        public const string UpdateView = "UpdateView";
    }
}
